docker run -ti --rm -v "$PWD":/home/gradle/project -w /home/gradle/project gradle:jdk18 bash

### events
 - addPotionEffect
 - dropItemNaturally
 - teleport
 - setFireTicks
 - strikeLightningEffect
 - addBan
 - spawnEntity

```java
    this.regularFortunes = new Fortune[] {
new Fortune("#f51c6a",\n"Reply hazy,\ntry again"),
new Fortune("#e7890c", "Good Luck"),
new Fortune("#bac200", "Average Luck"), new Fortune("#7fec11", "Bad Luck", player -> {
            player.addPotionEffect(new PotionEffect(PotionEffectType.UNLUCK, 2400, 0));
            double s = Math.random();
            Location loco = player.getLocation();
            if (s >= 0.9D) {
              Location centerOfBlock = loco.add(0.0D, 0.5D, 0.0D);
              loco.getWorld().dropItemNaturally(centerOfBlock, new ItemStack(Material.COBBLESTONE, 400));
            } else if (s >= 0.5D) {
              player.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 400, 0));
            } else if (s >= 0.3D) {
              player.teleport(loco.add(0.0D, 103.0D, 0.0D));
            } else {
              player.setFireTicks(500);
            }
          }),
new Fortune("#43fd3b", "Good news will come to you by mail"),
new Fortune("#00cbb0", "!!!!"),
new Fortune("#2a56fb", "Better not tell you now"),
new Fortune("#6023f8", "Outlook good"),
new Fortune("#d302a7", "Godly Luck", player -> {
            player.addPotionEffect(new PotionEffect(PotionEffectType.LUCK, 6000, 0));
            double s = Math.random();
            Location locoG = player.getLocation();
            if (s >= 0.9D) {
              ItemStack pcake = new ItemStack(Material.CAKE, 1);
              pcake.getItemMeta().setDisplayName(ChatColor.LIGHT_PURPLE + "Pogu's Cake");
              locoG.getWorld().dropItemNaturally(locoG, pcake);
            } else if (s >= 0.5D) {
              ItemStack pfoot = new ItemStack(Material.RABBIT_FOOT, 1);
              pfoot.getItemMeta().setDisplayName(ChatColor.LIGHT_PURPLE + "Pippa's FOOT");
              locoG.getWorld().dropItemNaturally(locoG, pfoot);
            } else if (s >= 0.3D) {
              ItemStack gN = new ItemStack(Material.GOLD_NUGGET, 1);
              gN.getItemMeta().setDisplayName(ChatColor.YELLOW + "Rabi's Blessing");
              locoG.getWorld().dropItemNaturally(locoG, gN);
            } else {
              locoG.getWorld().strikeLightningEffect(locoG);
              player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 6000, 0));
            }
          }) };
    this.specialFortunes = new Fortune[] {
        new Fortune("#ff0000", "(YOU ARE BANNED)", player -> {
            int onehr = 3600000;
            int time = (int)Math.floor((Math.random() < 0.1D) ? ((Math.random() < 0.1D) ? (8 * onehr) : onehr) : 600000.0D);
            Date banEnd = new Date(System.currentTimeMillis() + time);
            Bukkit.getBanList(BanList.Type.NAME).addBan(player.getName(), ChatColor.BOLD + "" + ChatColor.RED + "Your fortune: (YOU ARE BANNED)" + ChatColor.RESET, banEnd, "s4sfortune");
            player.kickPlayer(ChatColor.BOLD + "" + ChatColor.RED + "Your fortune: (YOU ARE BANNED)");
          }), new Fortune("#f7dc6f", "Your Shitty Luck Has Angered ZESUCHAMA", player -> {
            Location loc = player.getLocation();
            loc.getWorld().strikeLightning(loc);
          }), new Fortune("#fff451", "THEY GLOW IN THE DARK", player -> {
            player.addPotionEffect(new PotionEffect(PotionEffectType.GLOWING, 6000, 0));
            player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 6000, 0));
          }), new Fortune("#396a24", "*BRAAAAAAP* You Feel a Strange Smell around you ", player -> player.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 400, 0))),
          new Fortune("#612d5a", "You meet a dark handsome stranger ", player -> {
            Location locc = player.getLocation();
            locc.getWorld().spawnEntity(locc.add(0.0D, 1.0D, 0.0D), EntityType.ENDERMAN);
          }), new Fortune("#ffd700", "Oy Vey", player -> {
            Location locc = player.getLocation();
            locc.getWorld().spawnEntity(locc.add(0.0D, 2.0D, 5.0D), EntityType.WANDERING_TRADER);
          }), new Fortune("#00d7ff", "sixtee four diamonds ebin :DD", player -> {
            Location loc = player.getLocation();
            Location centerOfBlock = loc.add(0.0D, 0.5D, 0.0D);
            loc.getWorld().dropItemNaturally(centerOfBlock, new ItemStack(Material.DIAMOND, 64));
          }), new Fortune("#b48905", "Berries :D", player -> {
            Location loc = player.getLocation();
            Location centerOfBlock = loc.add(0.0D, 0.5D, 0.0D);
            ItemStack item = new ItemStack(Material.SWEET_BERRIES, 64);
            ItemMeta im = item.getItemMeta();
            im.setDisplayName(ChatColor.RED + "displayName");
            List<String> loreList = new ArrayList<>();
            loreList.add(ChatColor.BOLD + "Berry");
            loreList.add(ChatColor.GRAY + "Its a berry");
            im.setLore(loreList);
            item.setItemMeta(im);
            loc.getWorld().dropItemNaturally(centerOfBlock, item);
          }), new Fortune("#2600d0", "le ebin dubs xDDDDDDDDDDDD"),
          new Fortune("#5659fb", "you gon' get some dick", player -> {
            Location locc = player.getLocation();
            locc.getWorld().spawnEntity(locc.add(0.0D, 4.0D, 0.0D), EntityType.WITCH);
          }),
        new Fortune("#ec44e3", "ayy lmao"),
        new Fortune("#68923a", "Get Shrekt", player -> player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 600, 0))),
        new Fortune("#8c8c8c", "YOU JUST LOST THE GAME"),
        new Fortune("#a922ca", "NOT SO SENPAI BAKA~KUN") };

 .adverts = new String[] {
"secret erp channel",
"chatroom where you say ogey rrat",
"ONGOING RAID (jk)",
"be nice!",
"pekoland war room",
"takocon " + Calendar.getInstance().get(1),
"kiara fan club",
"server chat yah!",
"fumo posting area",
"ඞ",
"pharmacy",
"\"\"\"appeal\"\"\" your ban ;) ;) ",
"PIPPA NOTICE ME",
"h-hey,
me too!",
"virtual youtuber enthusiasts group",
" おはよううううううううう！！！",
"SEXXXXX",
"do your reps!",
"rigger Hate!",
"cum",
"I HATE KIARA",
"Daily Reminder: Miko raped Pekora" };
```
