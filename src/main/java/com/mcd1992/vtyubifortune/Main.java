package com.mcd1992.vtyubifortune;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
    private static Main pluginInstance;
    private static Logger log;
    private static YamlConfiguration config;

    public HashMap<UUID, Cooldown> cooldownMap;
    public double currentPlayerCount;
    public double maxPlayerCount;
    double currentPlayerCapacity;

    public String helpListString;

    public boolean configEnabled;
    public double configGlobalCooldown;


    @Override
    public void onEnable() {
        pluginInstance = this;
        log = getLogger();
        currentPlayerCount = 0;

        saveDefaultConfig();
        loadConfig();
        getServer().getPluginManager().registerEvents(new Listeners(this), this);
        PluginCommand cmd = getCommand("fortune");
        if (cmd instanceof PluginCommand) {
            Commands c = new Commands(this);
            cmd.setExecutor(c);
            cmd.setTabCompleter(c);
        }
    }

    @Override
    public void onDisable() {
        pluginInstance = null;
    }

    @Override
    public YamlConfiguration getConfig() {
        return config;
    }

    public Main getInstance() {
        return pluginInstance;
    }

    public void loadConfig() {
        cooldownMap = new HashMap<UUID, Cooldown>();
        maxPlayerCount = this.getServer().getMaxPlayers();
        helpListString = "";

        File configFolder = getDataFolder();
        File configFile = new File(configFolder, "config.yml");
        config = new YamlConfiguration();
        try {
            // Either SnakeYAML or bukkit/spigot's YAML doesn't understand what quotes are...
            // Even if you put a . in quotes it will still be parsed as nested keys. I love java devs.
            config.options().pathSeparator(';');
            config.loadFromString(Files.readString(Path.of(configFile.getPath())));
        } catch(Exception e) {
            log.severe(String.format("Failed to load config: %s", e.toString()));
            return;
        }

        // Generate default config if configVersion mismatch
        if (!config.isSet("configVersion") || config.getDouble("configVersion") != 1) {
            config.addDefault("configVersion", 1);
            config.addDefault("logLevel", "INFO");

            config.addDefault("enabled", true);
            config.addDefault("requirePrefix", false);
            config.addDefault("globalCooldown", 10.0);

            LinkedHashMap exampleFortunes = new LinkedHashMap();

            LinkedHashMap exampleBad = new LinkedHashMap();
            exampleBad.put("color", "#FFFFFF");
            exampleFortunes.put("Better not tell you now", exampleBad);

            LinkedHashMap exampleDark = new LinkedHashMap();
            LinkedHashMap exampleDarkEvents = new LinkedHashMap();
            exampleDark.put("color", "#FFFFFF");
            exampleDarkEvents.put("spawnMob", "minecraft:enderman");
            exampleDark.put("events", exampleDarkEvents);
            exampleFortunes.put("You meet a tall dark stranger", exampleDark);

            config.addDefault("fortunes", exampleFortunes);

            config.options().header("""
                Example Fortunes

                fortunes:
                  'Better not tell you now':
                    color: #00FF00
                  'You meet a tall dark stranger':
                    color: #000000
                    events:
                      spawnMob: minecraft:enderman
            """).copyDefaults(true);
            try {
                config.save(configFile);
                log.info("Created default config at: " + configFile.toString());
            } catch (IOException err) {
                log.warning(err.toString());
            }
        }
        log.info(String.format("Loaded config:\n%s\n\n", config.saveToString()));

        log.setLevel(Level.parse(config.getString("logLevel", "INFO")));
        configEnabled = config.getBoolean("enabled", true);
        configGlobalCooldown = config.getDouble("globalCooldown", 10.0);

        Map<String,Object> fortunes;
        MemorySection cfgFortunes = (MemorySection)config.get("fortunes", null);
        if (cfgFortunes instanceof MemorySection) {
            fortunes = cfgFortunes.getValues(false);
        } else {
            log.warning("No fortunes found in config");
            return;
        }

        for (String fortuneName : fortunes.keySet()) {
            var fortuneInfo = fortunes.get(fortuneName);
            log.info(fortuneInfo.toString());
            /*try {
                if (triggerInfo instanceof String triggerStr) { // register simple sound
                    sound = new Sound(soundName, triggerStr);
                } else if (triggerInfo instanceof MemorySection options) {
                    sound = new Sound(soundName, options.getValues(false));
                } else {
                    log.warning(String.format(
                        "Invalid options for sound %s: [%s] %s",
                        soundName, triggerInfo.getClass().toString(),
                        triggerInfo.toString())
                    );
                }
                helpListString = helpListString.trim() + " ";
                helpListString += (sound != null ? sound.helpText() : "");
                helpListString = helpListString.trim();
                log.fine("added sound: " + soundName);
            } catch(Exception e) {
                log.warning("Failed to add sound, invalid regex or options? " + soundName);
                log.warning(e.toString());
                continue;
            }

            if (sound != null && soundsMap.putIfAbsent(soundName, sound) != null) {
                log.warning("Duplicate sound in config: " + soundName);
            }*/
        }
    }
}
