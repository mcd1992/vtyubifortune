package com.mcd1992.vtyubifortune;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor, TabCompleter {
    private static Main pluginInstance;
    private static Logger log;

    private static ArrayList tabcomplete_empty;
    private static List tabcomplete_subcommands;

    Commands(Main instance) {
        pluginInstance = instance;
        log = instance.getLogger();
        tabcomplete_empty = new ArrayList<String>();
        tabcomplete_subcommands = Arrays.asList("reload");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        boolean isPlayer = sender instanceof Player;
        String output = "";

        if (args.length > 0) {
            switch (args[0]) {
                case "reload" -> {
                    if (sender.isOp()) {
                        pluginInstance.loadConfig();
                        output = "Configuration reloaded.";
                    } else {
                        // TODO: luckperms
                        output = "Console or OP only.";
                    }
                }
                case "debug" -> {
                    if (!isPlayer && sender.isOp()) { // console only
                        log.warning(String.format("config: \n%s\n", pluginInstance.getConfig().saveToString()));
                        log.warning(String.format("cooldownMap: %s\n", pluginInstance.cooldownMap.toString()));
                    } else {
                        return false;
                    }
                }
                default -> {
                    log.info("default");
                    return false;
                }
            }
        } else {
            // roll fortune
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "say todo roll " + sender.getName() + " " + ((Player)sender).getUniqueId());
            // 00000000-0000-0000-0000-000000000000
        }

        if (isPlayer) {
            Player player = (Player)sender;
            player.sendMessage(String.format("%s[%s] %s%s",
                ChatColor.AQUA, pluginInstance.getName(), output, ChatColor.RESET
            ));
        } else {
            sender.sendMessage(output);
            //log.info(output);
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        if (args.length < 2) {
            return tabcomplete_subcommands;
        }
        return tabcomplete_empty;
    }
}
