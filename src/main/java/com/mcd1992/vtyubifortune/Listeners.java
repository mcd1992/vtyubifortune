package com.mcd1992.vtyubifortune;
import java.time.Instant;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Listeners implements Listener {
    private static Main pluginInstance;
    private static Logger log;

    Listeners(Main instance) {
        pluginInstance = instance;
        log = instance.getLogger();
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Server server = pluginInstance.getServer();
        pluginInstance.currentPlayerCount = server.getOnlinePlayers().size();
        pluginInstance.maxPlayerCount = server.getMaxPlayers();
        pluginInstance.currentPlayerCapacity = Math.min(1.0, Math.abs(pluginInstance.currentPlayerCount / pluginInstance.maxPlayerCount));
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Server server = pluginInstance.getServer();
        pluginInstance.currentPlayerCount = server.getOnlinePlayers().size();
        pluginInstance.maxPlayerCount = server.getMaxPlayers();
        pluginInstance.currentPlayerCapacity = Math.min(1.0, Math.abs(pluginInstance.currentPlayerCount / pluginInstance.maxPlayerCount));
    }

    /*@EventHandler(priority=EventPriority.MONITOR, ignoreCancelled=true)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (!pluginInstance.configEnabled) return; // Plugin config says disable
        if (!event.isAsynchronous()) return; // Only players should trigger sounds

        Player ply = event.getPlayer();
        UUID plyUUID = ply.getUniqueId();
        String msg = event.getMessage().toLowerCase();
        boolean shouldHide = false;
        boolean isLocal = false;
        if (pluginInstance.configLocalHidePrefix instanceof String) {
            isLocal = msg.startsWith(pluginInstance.configLocalHidePrefix);
            if (isLocal) {
                msg = msg.substring(pluginInstance.configLocalHidePrefix.length());
                shouldHide = true;
            }
        }
        if (!isLocal && pluginInstance.configGlobalHidePrefix instanceof String) {
            shouldHide = msg.startsWith(pluginInstance.configGlobalHidePrefix);
            if (shouldHide) msg = msg.substring(pluginInstance.configGlobalHidePrefix.length());
        }
        if (pluginInstance.configRequirePrefix && !shouldHide) return;

        Cooldown plyCooldown = pluginInstance.cooldownMap.getOrDefault(plyUUID, new Cooldown());
        long curTime = Instant.now().toEpochMilli();
        boolean match = false;
        for (Map.Entry<String, Sound> entry : pluginInstance.soundsMap.entrySet()) {
            String soundName = entry.getKey();
            Sound sound = entry.getValue();
            match = sound.match(msg);

            if (match) {
                boolean onCooldown = false;
                if (isLocal) {
                    onCooldown = curTime < plyCooldown.local;
                } else {
                    onCooldown = curTime < plyCooldown.global;
                }
                if (onCooldown) {
                    double nextPlay = isLocal ? plyCooldown.local : plyCooldown.global;
                    ply.sendMessage(String.format(
                        "%s[%s] You must wait %s more seconds before another %s chat sound.%s",
                        ChatColor.RED, pluginInstance.getName(),
                        (Math.round((nextPlay - curTime) / 1000L) + 1),
                        isLocal ? "local" : "global",
                        ChatColor.RESET
                    ));
                    if (shouldHide) event.setCancelled(true);
                    return;
                }

                log.info(String.format("%s played %s %s", ply.getName(), soundName, isLocal ? "locally" : "globally"));
                if (isLocal) {
                    long delay = Math.round(Math.max(
                        pluginInstance.configLocalMinCooldown,
                        pluginInstance.configLocalMaxCooldown*
                        pluginInstance.currentPlayerCapacity
                    )*1000);
                    plyCooldown.local = Instant.now().toEpochMilli() + delay;
                    pluginInstance.cooldownMap.put(plyUUID, plyCooldown);

                    // Sounds must be mono for spatial effects https://bugs.mojang.com/browse/MC-146721
                    float volume = pluginInstance.configLocalVolume;
                    ply.getWorld().playSound(
                            ply.getLocation(),
                            ((pluginInstance.configLocalSoundSuffix instanceof String) ?
                             soundName + pluginInstance.configLocalSoundSuffix : soundName),
                            sound.category,
                            volume,
                            sound.pitch
                    );
                } else {
                    long delay = Math.round(Math.max(
                        pluginInstance.configGlobalMinCooldown,
                        pluginInstance.configGlobalMaxCooldown*
                        pluginInstance.currentPlayerCapacity
                    )*1000);
                    plyCooldown.global = Instant.now().toEpochMilli() + delay;
                    pluginInstance.cooldownMap.put(plyUUID, plyCooldown);

                    float volume = pluginInstance.configGlobalVolume;
                    for (Player p : pluginInstance.getServer().getOnlinePlayers()) {
                        Location pos = p.getLocation();
                        if (pluginInstance.configGlobalSoundSuffix instanceof String) {
                            // Dumb hack because mono sounds are always spatial
                            pos.setY(512);
                        }
                        p.playSound(
                                pos,
                                ((pluginInstance.configGlobalSoundSuffix instanceof String) ?
                                 soundName + pluginInstance.configGlobalSoundSuffix : soundName),
                                sound.category,
                                volume,
                                sound.pitch
                        );
                    }
                }
                if (shouldHide || isLocal) event.setCancelled(true);
                break;
            }
        }
        if (!match && (shouldHide || isLocal)) {
            ply.sendMessage(String.format(
                "%s[%s] Invalid sound.%s",
                ChatColor.RED, pluginInstance.getName(),
                ChatColor.RESET
            ));
            event.setCancelled(true);
        }
    }*/
}
